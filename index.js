const toggleEl = document.getElementById('toggle');
const basicAmount = document.getElementById('basicPlanAmount');
const professionalAmount = document.getElementById('professionalPlanAmount')
const masterAmount = document.getElementById('masterPlanAmount')


toggleEl.addEventListener('click', function () {

    if (toggleEl.checked) {

        basicAmount.textContent = '19.99'
        professionalAmount.textContent = '24.99'
        masterAmount.textContent = '34.99'

    }

    else {

        basicAmount.textContent = '199.99'
        professionalAmount.textContent = '249.99'
        masterAmount.textContent = '349.99'

    }
});